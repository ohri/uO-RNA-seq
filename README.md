# TMM 4910 H Course info (temporary)

This is a temporary location for class file until the Brightspace account is configured

## Files for class 1
* [TMM 4910 H Syllabus](public/TMM4910Hsyllbus.docx)
* [Code walkthrough](public/TMM4910H_day1.Rmd)
* [R library installations](public/libraryinstalls.txt)
* [Class 1 Presentation](public/TMM4910H_Lecture1.pptx)
* [Example BibTex file](public/citations.bib)

## Homework for class 1

For Class 1 homework, you will apply what you have learned about R and RStudio to adapt the [tutorial linked here](https://www.dataquest.io/blog/load-clean-data-r-tidyverse/) to an RStudio knitr report.  The homework report should use knitr markdown for formatting, and include a citation to the source tidyverse tutorial.   This homework is due November 8th before class. 

please send an email to gpalidwor@ohri.ca (Gareth Palidwor) if you attended the first class.
